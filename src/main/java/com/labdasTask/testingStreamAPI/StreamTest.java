package com.labdasTask.testingStreamAPI;

import java.util.ArrayList;
import java.util.IntSummaryStatistics;
import java.util.List;
import java.util.Optional;
import java.util.OptionalDouble;
import java.util.OptionalInt;

public class StreamTest{

  public static void main(String[] args) {
    List<Integer> list = new ArrayList<>();
    list.add(1);
    list.add(30);
    list.add(15);
    list.add(44);
    list.add(91);
    list.add(12);
    list.add(39);
    list.add(4);
    OptionalDouble average = list.stream().mapToInt(num -> num).average();
    System.out.println(average);
    OptionalInt min = list.stream().mapToInt(num -> num).min();
    System.out.println(min);
    Optional max = list.stream().max(Integer::compare);
    System.out.println(max);
    int sum = list.stream().mapToInt(n -> n).sum();
    System.out.println(sum);
    Optional<Integer> reduceSum = list.stream().reduce((n1, n2) -> n1 + n2);
    System.out.println(reduceSum);
    IntSummaryStatistics intSummaryStatistics = list.stream().mapToInt(n -> n).summaryStatistics();
    System.out.println(intSummaryStatistics);
    long count = list.stream()
        .filter(n -> n > (list.stream()
            .mapToInt(n1 -> n1)
            .sum() / list.size()))
        .count();
    System.out.println(count);


  }
}
