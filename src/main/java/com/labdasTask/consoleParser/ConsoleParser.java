package com.labdasTask.consoleParser;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class ConsoleParser {

  public static void main(String[] args) {
    List<String> list = new LinkedList<>();
    Scanner sc = new Scanner(System.in);
    boolean isNext = true;
    while (isNext){
      String s = sc.nextLine();
      if (s.equals("")){
        isNext = false;
      }else {
        list.add(s);
      }
    }
    //System.out.println(list.stream().distinct().count());
    long countUniqueWords = list.stream()
        .map(text -> text.split(""))
        .flatMap(Arrays::stream)
        .distinct()
        .count();
    //System.out.println(countUniqueWords);
    List<String> sortedUniqueWords = list.stream()
        .map(text -> text.split(""))
        .flatMap(Arrays::stream)
        .distinct()
        .sorted()
        .collect(Collectors.toList());
    //System.out.println(sortedUniqueWords);
    Map<String, Long> countEveryWord = list.stream()
        .map(text -> text.split(""))
        .flatMap(Arrays::stream)
        .collect(Collectors.groupingBy(word -> word, Collectors.counting()));
    //System.out.println(countEveryWord);
    Map<Character, Long> countEveryWordExceptCapital = list.stream()
        .flatMap(text -> text.codePoints().mapToObj(c -> (char)c))
        .filter(Character::isLowerCase)
        .collect(Collectors.groupingBy(c -> c, Collectors.counting()));
    System.out.println(countEveryWordExceptCapital);
  }
}
