package com.labdasTask.commandPatternTest;

import java.util.Arrays;
import java.util.Scanner;

public class TestCommand {

  public static void main(String[] args) {
    View view = new View();
ObjectService objectService = new ObjectService();

  boolean isRunning = true;
  Scanner sc = new Scanner(System.in);
    while (isRunning){
      view.menu();
      int choice = sc.nextInt();
      String arg = sc.next();
      switch (choice){
        case 1:
          objectService.reverse(arg);
          break;
        case 2:
         Command command = new Command(){
            @Override
            public void execute(String s) {
              System.out.println(arg + " from anonymous class");
            }
          };
         command.execute(arg);
          break;
        case 3:
          Command newArg = s -> System.out.println(s.concat(" by lambda"));
          newArg.execute(arg);
          break;
        case 4:
          Command changedArg = System.out::println;
          changedArg.execute(arg);
          break;
        case 5:
          isRunning = false;
          break;
          default:
            System.out.println("Choose correct number");
            break;
      }
    }

  }

}
