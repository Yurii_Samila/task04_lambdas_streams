package com.labdasTask.commandPatternTest;

public class View {
  public void menu(){
    System.out.println("Please, choose an option:\n"
        + "1) Use object reference\n"
        + "2) Use anonymous interface\n"
        + "3) Use method reference\n"
        + "4) Use lambda expresion\n"
        + "5) Exit");
  }

}
