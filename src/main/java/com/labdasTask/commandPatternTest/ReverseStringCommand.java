package com.labdasTask.commandPatternTest;

public class ReverseStringCommand implements Command {

  private ObjectService objectService;

  public void reverse(ObjectService objectService){
    this.objectService = objectService;
  }

  @Override
  public void execute(String s) {
  objectService.reverse(s);
  }
}
