package com.labdasTask.commandPatternTest;

public class CommandInvoker {
  private Command reverseCommand;

  public CommandInvoker(Command reverseCommand){
    this.reverseCommand = reverseCommand;
  }

  public void reverse(String s){
    reverseCommand.execute(s);
  }

}
