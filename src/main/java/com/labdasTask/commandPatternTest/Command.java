package com.labdasTask.commandPatternTest;

public interface Command {
  void execute(String s);
}
