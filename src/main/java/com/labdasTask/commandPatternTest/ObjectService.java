package com.labdasTask.commandPatternTest;

public class ObjectService {
public void reverse(String s){
  char[] chars = s.toCharArray();
  char[] reverseChar = new char[s.length()];
  for (int i = 0; i < chars.length; i++) {
    reverseChar[chars.length - 1 - i] = chars[i];
  }
  System.out.println(String.valueOf(reverseChar));
}
}
