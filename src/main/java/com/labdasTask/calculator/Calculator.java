package com.labdasTask.calculator;

public interface Calculator {
    int calculation(int a, int b, int c);
}
